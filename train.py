import numpy as np
import pandas as pd
import tensorflow as tf
import time

NAME = f'COVID-19#{int(time.time())}'
BATCH_SIZE = 1
TRAIN_SPLIT = 75
HISTORY = 2
TARGET = 10


def prepare_data(data, start_index, end_index, history_size=HISTORY, target_size=TARGET):
    values = []
    labels = []
    start_index = start_index + history_size
    if end_index is None:
        end_index = len(data) - target_size
    for i in range(start_index, end_index):
        indices = range(i - history_size, i)
        values.append(np.reshape(data[indices], (history_size, 1)))
        labels.append(data[i:i + target_size])
    return np.array(values), np.array(labels)


data = pd.read_csv('covid_19_data.csv')

country_data = data[data['Country/Region'] == 'Hong Kong']['Confirmed'].values
TRAIN_SPLIT = int(len(country_data) * TRAIN_SPLIT / 100)
mean = country_data[:TRAIN_SPLIT].mean()
std = country_data[:TRAIN_SPLIT].std()
data = (country_data - mean) / std

x_train, y_train = prepare_data(data, 0, TRAIN_SPLIT)
x_test, y_test = prepare_data(data, TRAIN_SPLIT, None)

train_data = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_data = train_data.cache().batch(BATCH_SIZE).repeat()

validation_data = tf.data.Dataset.from_tensor_slices((x_test, y_test))
validation_data = validation_data.batch(BATCH_SIZE).repeat()

lstm_model = tf.keras.models.Sequential([
    tf.keras.layers.LSTM(8, input_shape=x_train.shape[-2:], activation='relu'),
    tf.keras.layers.Dense(TARGET)
])

lstm_model.compile(optimizer='adam', loss='mae')

tensor_board = tf.keras.callbacks.TensorBoard(log_dir=f'logs\\{NAME}')
early_stopping = tf.keras.callbacks.EarlyStopping(patience=150, verbose=1, restore_best_weights=True)
lstm_model.fit(train_data, epochs=5000, steps_per_epoch=5, validation_data=validation_data, validation_steps=2,
               callbacks=[tensor_board, early_stopping])
lstm_model.save('model.h5')
