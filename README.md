# Sławomir Olszewski 165713
Model został wytrenowany przy użyciu sieci LSTM. Do wytrenowania użyłem danych dotyczących ilości 
otwierdzonych przypadków w Hong Kongu, sieć zwraca w miarę sensowne wyniki bazując na tych danych.

Ciężko ocenić jak bardzo miarodajna jest predykcja, gdyż sposób przyrostu zarażeń w 
danym regionie nie jest regulowany tylko i wyłącznie liczbą aktualnych zarażeń.

Na obrazie zaprezentowana jest predyckja sytuacji w Hong Kongu w przeciągu kolejnych 10 dni
(zaczynając od 23.03.2020).

![alt text](prediction.png)

Korzystałem z danych zamieszczonych na stronie https://www.kaggle.com/sudalairajkumar/novel-corona-virus-2019-dataset, z pliku covid_19_data.csv.
