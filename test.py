import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, timedelta
import pandas as pd

model = tf.keras.models.load_model('model.h5')

df = pd.read_csv('covid_19_data.csv')
test_data = df[df['Country/Region'] == 'Hong Kong']['Confirmed'].values[-30:]
dates = df[df['Country/Region'] == 'Hong Kong']['ObservationDate']
dates = pd.to_datetime(dates).values[-30:]

samples = test_data[-2:]
output = model.predict([[[samples[0]], [samples[1]]]])
output = output.flatten()

output = [int(x) for x in output]
dates_pred = np.asarray([datetime.strptime(str(dates[-1]).split('T')[0], '%Y-%m-%d') + timedelta(days=x) for x in range(len(output))])

plt.title('Hong Kong Covid-19 Infection Prediction')
plt.xlabel('Date')
plt.ylabel('Confirmed Cases')
plt.plot(dates, test_data, label='Test Data', color='red')
plt.plot(dates_pred, output, '-go', label='Prediction')
plt.legend(loc='upper left')
plt.show()
